#include "PCD8544.h"

PCD8544::PCD8544(HardwareSpi *spi, Gpio *rst, Gpio *dc, Gpio *backlight)
    : _spi(spi), _rst(rst), _dc(dc), _backlight(backlight)
{
  // Configure spi speed at 4MHz
  _spi->setSpeed(4000000);

  // Setup the LCD controller
  _setup();
}

PCD8544::~PCD8544()
{
  // Reset the LCD controller
  _reset();
}

void PCD8544::refresh()
{
  // Set the lcd memory cursors
  _writeCommand(PCD8544_Y_ADDR | 0);
  _writeCommand(PCD8544_X_ADDR | 0);

  // Write the data
  for (uint32_q i = 0; i < sizeof(_buffer); i++)
    _writeData(_buffer[i]);
}

void PCD8544::partialRefresh()
{
  // Iterate through the affected rows
  for (int32_q row = _boundingBox.ymin; row <= _boundingBox.ymax; row++)
  {
    // Set the lcd memory row cursor
    _writeCommand(PCD8544_Y_ADDR | row);

    // Set the lcd memory column cursor
    uint32_q column = _boundingBox.xmin;
    _writeCommand(PCD8544_X_ADDR | column);

    // Write the data
    for (; column <= _boundingBox.xmax; column++)
      _writeData(_buffer[column + row * PCD8544_WIDTH]);
  }
}

void PCD8544::print(string str, uint8_q row, uint8_q xPosition)
{
  // Print all characters of the string
  for (uint32_q i = 0; i < str.length(); i++)
  {
    print(str.c_str()[i], row, xPosition);
    xPosition++;
  }

  // Check if the string is shorter or equal the max. string width
  // that fits into a row.
  // ..if yes: Mark the target row for update
  // ..if  no: Mark the whole screen for update
  if (str.length() <= 14)
    _updateBoundingBox(0, PCD8544_WIDTH - 1, row, row);
  else
    _updateBoundingBox(0, PCD8544_WIDTH - 1, 0, PCD8544_HEIGHT - 1);
}

void PCD8544::print(byte_q chr, uint8_q row, uint8_q xPosition)
{
  // Calculate the start position
  // Character with: 6
  uint32_q start = xPosition * 6 + row * PCD8544_WIDTH;

  // Get the real offset value in the ascii table
  uint32_q asciiOffset = chr - 0x20;

  // Copy all 6 bytes (representing the character)
  for (int32_q i = 0; i < 6; i++)
    _buffer[start + i] = ascii[asciiOffset][i];

  // Only mark the character area for update
  _updateBoundingBox(xPosition, xPosition + 6, row, row);
}

void PCD8544::printRow(const byte_q *content, uint8_q row)
{
  // Get the content length
  uint32_q length = std::char_traits<byte_q>::length(content);

  // Copy the content data to the buffer
  for (uint32_q i = 0; i < length; i++)
    _buffer[i + row * PCD8544_WIDTH] = content[i];

  // Only mark the row for update
  _updateBoundingBox(0, PCD8544_WIDTH - 1, row, row);
}

void PCD8544::drawVerticalLine(uint8_q xPosition, uint8_q startRow, uint8_q endRow)
{
  // Draw the vertical line
  for (int32_q row = startRow; row <= endRow; row++)
    _buffer[xPosition + row * PCD8544_WIDTH] = 0xFF;

  // Only mark the vertical line area for update
  _updateBoundingBox(xPosition, xPosition, 0, 4);
}

void PCD8544::removeVerticalLine(uint8_q xPosition, uint8_q startRow, uint8_q endRow)
{
  // Remove the vertical line
  for (int32_q row = startRow; row <= endRow; row++)
    _buffer[xPosition + row * PCD8544_WIDTH] = 0x00;

  // Only mark the vertical line area for update
  _updateBoundingBox(xPosition, xPosition, 0, 4);
}

void PCD8544::decreaseVerticalLineHeight(uint8_q xPosition, uint8_q startRow, uint8_q endRow)
{
  // Needed for partial update
  int32_q currentRow = -1;

  // Iterate through all rows
  for (int32_q row = startRow; row <= endRow; row++)
  {
    // Calculate the offset for the target byte
    uint32_q offset = xPosition + row * PCD8544_WIDTH;

    // Check if there is anything set in the byte
    if (_buffer[offset] > 0x00)
    {
      // Iterate through all bits of the current byte
      for (int32_q shift = 0; shift <= 7; shift++)
      {
        // Look for the lowest bit that is set to '1' (if there is one)
        if ((_buffer[offset] & (1 << shift)) > 0)
        {
          // Set current row
          currentRow = row;

          // Set it to '0' and break out
          _buffer[offset] &= ~(1 << shift);
          break;
        }
      }

      break;
    }
  }

  // Only mark the vertical line area for update
  if (currentRow == -1)
    _updateBoundingBox(xPosition, xPosition, 0, 4);
  else
    _updateBoundingBox(xPosition, xPosition, currentRow, currentRow);
}

void PCD8544::increaseVerticalLineHeight(uint8_q xPosition, uint8_q startRow, uint8_q endRow)
{
  // Needed for partial update
  int32_q currentRow = -1;

  // Iterate through all rows
  for (int32_q row = startRow; row >= endRow; row--)
  {
    // Calculate the offset for the target byte
    uint32_q offset = xPosition + row * PCD8544_WIDTH;

    // Check if there is any zero bit in the target byte
    if (_buffer[offset] < 0xFF)
    {
      // Iterate through all bits of the current byte
      for (int32_q shift = 7; shift >= 0; shift--)
      {
        // Look for the highest bit that is set to '0' (if there is one)
        if ((_buffer[offset] & (1 << shift)) == 0)
        {
          // Set current row
          currentRow = row;

          // Set it to '1' and break out
          _buffer[offset] |= (1 << shift);
          break;
        }
      }

      break;
    }
  }

  // Only mark the vertical line area for update
  if (currentRow == -1)
    _updateBoundingBox(xPosition, xPosition, 0, 4);
  else
    _updateBoundingBox(xPosition, xPosition, currentRow, currentRow);
}

void PCD8544::_setup()
{
  // Perform a reset
  _reset();

  // Enable extended instruction mode
  _writeCommand(PCD8544_FUNCTION_SET | PCD8544_EXTENDED_INSTRUCTION);

  // Select bias system
  // 0x04 works fine after some testing
  _writeCommand(PCD8544_BIAS_SYSTEM | 0x04);

  // Set the temperature coefficient
  // PCD8544_TEMP_COEFF_0 works fine after some testing
  _writeCommand(PCD8544_TEMPERATURE_CTRL | PCD8544_TEMP_COEFF_0);

  // Set VOP (contrast)
  // Contrast values are from 0x00 to 0x7F
  // 0x39 works fine after some testing
  _writeCommand(PCD8544_VOP | 0x39);

  // Reset to normal instruction mode
  _writeCommand(PCD8544_FUNCTION_SET);

  // Set display to normal
  _writeCommand(PCD8544_DISPLAY_CTRL | PCD8544_DISPLAY_NORMAL);

  // Mark the whole screen for update
  _updateBoundingBox(0, PCD8544_WIDTH - 1, 0, PCD8544_HEIGHT - 1);

  // Clear and refresh the display
  _clear();
  refresh();
}

void PCD8544::_writeCommand(byte_q command)
{
  // Set to command mode
  _dc->set(low);

  *_spi << command;
  _spi->transfer(0);
}

void PCD8544::_writeData(byte_q data)
{
  // Set to data mode
  _dc->set(high);

  *_spi << data;
  _spi->transfer(0);
}

void PCD8544::_clear()
{
  // Clear the buffer
  memset(_buffer, 0, sizeof(_buffer));

  // Mark the whole screen for update
  _updateBoundingBox(0, PCD8544_WIDTH - 1, 0, PCD8544_HEIGHT - 1);

  // Refresh the display
  refresh();
}

void PCD8544::_reset()
{
  //
  // RST is active low
  //
  // After reset the LCD driver state
  // has the following state:
  // PD = 1     (power down)
  //  V = 0     (horizontal addressing mode)
  //  E = D = 0 (display is blank)
  //  X = Y = 0 (X, Y address counter)
  //  T = 0     (temperature control mode)
  // BS = 0     (bias system)
  // Vo = 0     (hf generator off)
  //
  _rst->set(low);
  usleep(500 * 1000);
  _rst->set(high);
}

void PCD8544::_updateBoundingBox(uint8_q xmin, uint8_q xmax, uint8_q ymin, uint8_q ymax)
{
  // Set the bounding box positions
  _boundingBox.xmin = xmin;
  _boundingBox.xmax = xmax;
  _boundingBox.ymin = ymin;
  _boundingBox.ymax = ymax;
}
