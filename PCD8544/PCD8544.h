#ifndef PCD8544_H
#define PCD8544_H

//
// Project includes
//
#include <Bonewire/BonewireTypes.h>
#include <Bonewire/Spi/Hardware/HardwareSpi.h>
#include <Bonewire/Gpio/Gpio.h>
#include "Bitmaps.h"

//
// Defines
//
#define PCD8544_HEIGHT           48  // Pixel
#define PCD8544_WIDTH            84  // Pixel

#define PCD8544_FUNCTION_SET     0x20

// Default commands (H = 0)
#define PCD8544_DISPLAY_CTRL     0x08
#define PCD8544_X_ADDR           0x80
#define PCD8544_Y_ADDR           0x40

// Extended commands (H = 1)
#define PCD8544_TEMPERATURE_CTRL 0x04
#define PCD8544_BIAS_SYSTEM      0x10
#define PCD8544_VOP              0x80

// Bits
#define PCD8544_POWER_DOWN            0x04
#define PCD8544_VERTICAL_ADDRESSING   0x02
#define PCD8544_EXTENDED_INSTRUCTION  0x01

// Masks
#define PCD8544_DISPLAY_BLANK    0x00
#define PCD8544_DISPLAY_NORMAL   0x04
#define PCD8544_DISPLAY_ALL_ON   0x01
#define PCD8544_DISPLAY_INVERSE  0x05

#define PCD8544_TEMP_COEFF_0     0x00
#define PCD8544_TEMP_COEFF_1     0x01
#define PCD8544_TEMP_COEFF_2     0x02
#define PCD8544_TEMP_COEFF_3     0x03

//
// Struct
//
struct BoundingBox
{
  uint8_q xmin;
  uint8_q ymin;
  uint8_q xmax;
  uint8_q ymax;
};

//
// Using
//
using namespace Bonewire;

//
// Example usage
//
/*
 HardwareSpi spi(SPI_1_0);
 Gpio reset(GPIO_73, output);
 Gpio dc(GPIO_70, output);
 Gpio backlight(GPIO_71, output);

 backlight.set(high);

 PCD8544 pcd(&spi, &reset, &dc, &backlight);
 pcd.printRow(channelFooter, 5);
 pcd.refresh();

 while (true)
 {
 for (int i = 0; i < 84; i++)
 {
 int x = rand() % 2;

 if (x == 0)
 pcd.increaseVerticalLineHeight(i);
 else
 pcd.decreaseVerticalLineHeight(i);

 pcd.partialRefresh();
 }
 }
 */

class PCD8544
{
private:
  /**
   * SPI bus
   */
  HardwareSpi *_spi;

  /**
   * Reset gpio
   */
  Gpio *_rst;

  /**
   * Data / Command gpio
   */
  Gpio *_dc;

  /**
   * Backlight gpio
   */
  Gpio *_backlight;

  /**
   * Memory cursor position holder
   */
  BoundingBox _boundingBox;

  /**
   * LCD controller memory buffer
   */
  byte_q _buffer[(PCD8544_WIDTH * PCD8544_HEIGHT) / 8];

  /**
   * Setup the LCD controller.
   *
   * @return void
   */
  void _setup();

  /**
   * Write a command to the LCD controller.
   *
   * @param command Command
   * @return        void
   */
  void _writeCommand(byte_q command);

  /**
   * Write data to the LCD controller.
   *
   * @param data Data
   * @return     void
   */
  void _writeData(byte_q data);

  /**
   * Clear the LCD controller memory.
   *
   * @return void
   */
  void _clear();

  /**
   * Reset the LCD controller.
   *
   * @return void
   */
  void _reset();

  /**
   * Update the LCD controller's memory cursor position.
   *
   * @param xmin Min. x position
   * @param xmax Max. x position
   * @param ymin Min. y position
   * @param ymax Max. y position
   * @return     void
   */
  void _updateBoundingBox(uint8_q xmin, uint8_q xmax, uint8_q ymin, uint8_q ymax);

  /**
   * Copy constructor
   */
  PCD8544(PCD8544 &original);

  /**
   * Assignment operator
   */
  PCD8544 &operator=(const PCD8544 &source);

public:
  /**
   * Overloaded constructor
   */
  PCD8544(HardwareSpi *spi, Gpio *rst, Gpio *dc, Gpio *backlight);

  /**
   * Default destructor
   */
  virtual ~PCD8544();

  /**
   * Refresh the display.
   *
   * @return void
   */
  void refresh();

  /**
   * Refresh a part (holded by the bounding box) of the display.
   *
   * @return void
   */
  void partialRefresh();

  /**
   * Print a string in a target row at a specific position.
   *
   * @param str       String
   * @param row       Row
   * @param xPosition X position
   * @return          void
   */
  void print(string str, uint8_q row, uint8_q xPosition);

  /**
   * Print a character in a target row at a specific position.
   *
   * @param chr       Character
   * @param row       Row
   * @param xPosition X position
   * @return          void
   */
  void print(byte_q chr, uint8_q row, uint8_q xPosition);

  /**
   * Print a byte content to a target row.
   *
   * @param content Content as a byte array
   * @return        void
   */
  void printRow(const byte_q *content, uint8_q row);

  /**
   * Draw a vertical line at a specific position.
   *
   * @param xPosition X position
   * @param startRow  Start row
   * @param endRow    End row
   * @return          void
   */
  void drawVerticalLine(uint8_q xPosition, uint8_q startRow = 0, uint8_q endRow = 4);

  /**
   * Remove a vertical line at a specific position.
   *
   * @param xPosition X position
   * @param startRow  Start row
   * @param endRow    End row
   * @return          void
   */
  void removeVerticalLine(uint8_q xPosition, uint8_q startRow = 0, uint8_q endRow = 4);

  /**
   * Decrease the height of a vertical line at a specific position.
   *
   * @param xPosition X position
   * @param startRow  Start row
   * @param endRow    End row
   * @return          void
   */
  void decreaseVerticalLineHeight(uint8_q xPosition, uint8_q startRow = 0, uint8_q endRow = 4);

  /**
   * Increase the height of a vertical line at a specific position.
   *
   * @param xPosition X position
   * @param startRow  Start row
   * @param endRow    End row
   * @return          void
   */
  void increaseVerticalLineHeight(uint8_q xPosition, uint8_q startRow = 4, uint8_q endRow = 0);
};

#endif // PCD8544_H
